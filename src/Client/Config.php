<?php

namespace Spaaza\Client;

class Config
{
    public $authKey;
    public $authSecret;
    public $baseUrl;
    public $hostname;
    public $chainId;
    public $userAgent;
    public $connectTimeout = 5;
    public $readTimeout = 5;
    public $version = 'v1';

    public $apiVersion = '1.1.8';

    public function __construct(array $config = [])
    {
        // set the default user agent version
        $this->userAgent .= $this->getUserAgent();
        foreach ($config as $key => $value) {
            $property = $this->toCamelcase($key);
            $this->$property = $value;
        }
    }

    /**
     * Convert from underscore_words to camelCase
     *
     * @param string $str Source string in underscore_words *or* camelCase
     * @return string Camelcased version
     */
    protected function toCamelcase($str)
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $str))));
    }

    /**
     * Get the user agent of the client
     *
     * @return string
     */
    protected function getUserAgent()
    {
        return 'SpaazaPhpClient/' . \Spaaza\Client::VERSION;
    }
}
