<?php

namespace Spaaza\Client;

class ErrorsException extends \Exception
{
    public $name;
    public $description;

    protected $isFatal;

    public function __construct($error, $isFatal = null)
    {
        if (is_array($error) && count($error) > 0) {
            $code = array_keys($error)[0];
            parent::__construct($error[$code]['name'] . ': ' . $error[$code]['description'], $code);

            $this->name = $error[$code]['name'];
            $this->description = $error[$code]['description'];
        } else {
            $this->name = 'unknown_name';
            $this->description = (is_string($error) && strlen($error) > 0)
                ? $error
                : 'An unknown error has occurred while talking to the Spaaza API';
            parent::__construct($this->description, 0);
        }
        if ($isFatal !== null) {
            $this->isFatal = $isFatal;
        }
    }

    /**
     * Check if there is $name between the error names
     *
     * @param string $name
     * @return bool
     */
    public function hasName($name)
    {
        return in_array($name, $this->getNames());
    }

    /**
     * Get all error names
     *
     * For now, this object can only handle one error and Spaaza only seems to return only one error
     * at a time, but in the future maybe more errors can be parsed and then this function should look
     * in all errors.
     *
     * @return array
     */
    public function getNames()
    {
        return [$this->name];
    }

    /**
     * Is one of the errors fatal?
     *
     * @return bool
     */
    public function isFatal()
    {
        if (isset($this->isFatal) && is_bool($this->isFatal)) {
            return $this->isFatal;
        }
        $intersect = array_intersect($this->getNames(), [
            'user_not_found',
            'user_already_exists',
        ]);
        return count($intersect) > 0;
    }
}
