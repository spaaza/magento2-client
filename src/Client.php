<?php

namespace Spaaza;

use GuzzleHttp\Client as HttpClient;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Spaaza\Client\Config;
use Spaaza\Client\ErrorsException;

class Client
{
    const VERSION = '1.0.5';

    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';
    const METHOD_POST_JSON = 'POST_JSON';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';
    const METHOD_DELETE_JSON = 'DELETE_JSON';

    /**
     * @var Config
     */
    protected $config;

    /**
     * @var LoggerInterface
     */
    protected $log;

    /**
     * @var ResponseInterface
     */
    protected $lastResponse;

    public function __construct(LoggerInterface $log = null, Config $config = null)
    {
        if ($config) {
            $this->config = $config;
        }
        $this->log = $log;
    }

    /**
     * Replace the config object
     *
     * @param Config $config
     * @return $this
     */
    public function setConfig(Config $config)
    {
        $this->config = $config;
        return $this;
    }

    /**
     * Get the config object
     *
     * @return Config
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * Do the request to the webservice
     *
     * Options available:
     * - send_hostname  To send the configured hostname as a header. Required for some requests (see the API docs)
     * - send_chain_id  Add the configured chain id to the payload
     *
     * @param string $path
     * @param array  $payload  Request (parameters) in array format
     * @param string $method   One of self::METHOD_GET, self::METHOD_POST
     * @param array  $options  Additional options (see docs above)
     * @return array Decoded Json-array of the body
     */
    public function request($path, array $payload, $method = self::METHOD_GET, array $options = [])
    {
        if (!$options) {
            $options = [];
        }

        $client = $this->getHttpClient($this->config->baseUrl . $this->config->version . '/');
        $headers = $this->composeHeaders($options);

        if (!empty($options['send_chain_id'])) {
            $payload['chain_id'] = $this->config->chainId;
        }

        $requestOptions = [
            'headers' => $headers,
        ];

        $httpMethod = 'GET';
        switch ($method) {
            case self::METHOD_GET:
                $requestOptions['query'] = $payload;
                $httpMethod = 'GET';
                break;
            case self::METHOD_DELETE:
            case self::METHOD_DELETE_JSON:
                if ($method == self::METHOD_DELETE_JSON) {
                    $requestOptions['json'] = $payload;
                } else {
                    $requestOptions['query'] = $payload;
                }
                $httpMethod = 'DELETE';
                break;
            case self::METHOD_POST:
            case self::METHOD_POST_JSON:
                if ($method == self::METHOD_POST_JSON) {
                    $requestOptions['json'] = $payload;
                } else {
                    $requestOptions['form_params'] = $payload;
                }
                $httpMethod = 'POST';
                break;
            case self::METHOD_PUT:
                $requestOptions['json'] = $payload;
                $httpMethod = 'PUT';
                break;
        }

        $response = false;
        try {
            /** @var ResponseInterface $response */
            $response = $client->request($httpMethod, $path, $requestOptions);
            $this->setLastResponse($response);

            $body = json_decode($response->getBody(), true);
            return $this->processBody($body, $options);
        } catch (\Exception $e) {
            if ($this->log) {
                $this->log->error('Spaaza Request Error: ' . $e->getMessage());
            }
            throw $e;
        }
    }

    /**
     * Check and process the body and determine what to return
     *
     * @param array $body
     * @param array $requestOptions  The options passed to {@link request()}
     * @return mixed
     */
    protected function processBody($body, array $requestOptions)
    {
        $apiVersion = $this->useApiVersion($requestOptions);
        if (version_compare($apiVersion, '1.4.0', '>=')) {
            if (!empty($body['error'])) {
                throw new ErrorsException([ $body['error'] ]);
            }
        } else {
            if (!empty($body['errors'])) {
                throw new ErrorsException($body['errors']);
            }
        }

        if (isset($body['results'])) {
            return $body['results'];
        } elseif (!empty($body['result']['code']) && $body['result']['code'] == 1) {
            return true;
        } else {
            throw new \RuntimeException('Empty or unreadable body of Spaaza Api response');
        }
    }

    /**
     * Compose the HTTP headers based on default headers + options
     *
     * @param array $options
     * @return array
     */
    protected function composeHeaders(array $options)
    {
        $headers = [
            'User-Agent' => $this->config->userAgent,
            'Accept' => 'application/json',
            'Authorization' => sprintf('Bearer %s:%s', $this->config->authKey, $this->config->authSecret),
            'X-Spaaza-API-version' => $this->useApiVersion($options),
        ];

        if (!empty($options['send_hostname'])) {
            $headers['X-MyPrice-App-Hostname'] = $this->config->hostname;
        }
        if (!empty($options['on_behalf_of'])) {
            $headers['X-Spaaza-On-Behalf-Of'] = $options['on_behalf_of'];
        }

        return $headers;
    }

    /**
     * Determine which Spaaza API version to use
     *
     * @param array $requestOptions
     * @return string
     */
    protected function useApiVersion(array $requestOptions): string
    {
        $version = $this->config->apiVersion;
        if (!empty($requestOptions['api_version'])) {
            $version = $requestOptions['api_version'];
        }
        return $version;
    }

    /**
     * Get a new HTTP Client object
     *
     * @param string $baseUri
     * @return HttpClient
     */
    protected function getHttpClient($baseUri = '')
    {
        return new HttpClient(
            [
                'base_uri' => $baseUri,
                'connect_timeout' => $this->config->connectTimeout,
                'read_timeout' => $this->config->readTimeout,
                'timeout' => $this->config->readTimeout,
                'http_errors' => false,
            ]
        );
    }

    /**
     * @return ResponseInterface
     */
    public function getLastResponse()
    {
        return $this->lastResponse;
    }

    /**
     * @param ResponseInterface $lastResponse
     * @return $this
     */
    public function setLastResponse($lastResponse)
    {
        $this->lastResponse = $lastResponse;
        return $this;
    }
}
